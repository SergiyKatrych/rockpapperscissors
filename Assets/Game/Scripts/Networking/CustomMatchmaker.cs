﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon;
using System;

public class CustomMatchmaker : Photon.PunBehaviour
{

	[SerializeField] private Text loggerText;
	[SerializeField] private RoomListController roomListController;
	[SerializeField] private Button joinRoomButton;

	private RoomInfo[] _rooms;
	private RoomInfo _currentRoom;

	// Use this for initialization
	void Start()
	{
		PhotonNetwork.logLevel = PhotonLogLevel.Full;
		PhotonNetwork.ConnectUsingSettings(Application.version);
		roomListController.Init();
	}
	
	// Update is called once per frame
	void Update()
	{
		loggerText.text = PhotonNetwork.connectionStateDetailed.ToString() + " "+PhotonNetwork.room.name;
	}

	#region Button Callbacks
	public void CreateRoom(string roomName)
	{
		if (PhotonNetwork.CreateRoom(roomName))
		{
		}
		else
		{
			PhotonNetwork.CreateRoom(null);
		}
	}

	public void RefreshRooms()
	{
		_rooms = PhotonNetwork.GetRoomList();

		if (_rooms != null && _rooms.Length > 0)
		{
			joinRoomButton.interactable = true;
		}
		else
		{
			joinRoomButton.interactable = false;
			//joinRoomButton.interactable = true;
		}
		roomListController.SetRooms(_rooms);
	}

	public void JoinRoom()
	{
		if (IsJoinedLobby())
		{
			if (roomListController.SelectedRoom != null)
			{
				_currentRoom = roomListController.SelectedRoom;
				PhotonNetwork.JoinRoom(_currentRoom.name);
			}
			else
			{
				PhotonNetwork.JoinRandomRoom();
			}
		}
	}

	#endregion

	public override void OnJoinedLobby()
	{
		
	}

	public override void OnReceivedRoomListUpdate()
	{
		RefreshRooms();
	}

	public override void OnJoinedRoom()
	{
		Debug.Log("On Joined Room");
	}

	private bool IsJoinedLobby()
	{
		return PhotonNetwork.connectionStateDetailed == ClientState.JoinedLobby;
	}

	private void LogRooms()
	{
		foreach (var room in _rooms)
		{
			Debug.Log("room " + room);
		}
	}
}
