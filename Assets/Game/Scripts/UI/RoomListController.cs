﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomListController : MonoBehaviour
{
	[SerializeField] private RoomListItem itemPrefab;
	[SerializeField] private VerticalLayoutGroup itemsRoot;

	private List<RoomListItem> _allRooms;
	private RoomListItem _selectedRoom;

	public void Init()
	{
		ClearAllChildren();
	}

	public void SetRooms(RoomInfo[] roomsList)
	{
		ClearAllChildren();

		if (roomsList != null)
		{
			_allRooms = new List<RoomListItem>();
			foreach (var room in roomsList)
			{
				RoomListItem item = Instantiate(itemPrefab);
				_allRooms.Add(item);
				item.transform.SetParent(itemsRoot.transform, false);
				item.Init(room, this);
			}

			if (_allRooms.Count > 0)
			{
				_allRooms[0].GetComponent<Button>().onClick.Invoke();
			}
		}
	}

	private void ClearAllChildren()
	{
		_selectedRoom = null;
		_allRooms = null;
		for (int i = itemsRoot.transform.childCount; i > 0; i--)
		{
			GameObject.Destroy(itemsRoot.transform.GetChild(i-1).gameObject);
		}
	}

	public void OnSelectedRoom(RoomListItem selectedRoom)
	{
		_selectedRoom = selectedRoom;
		foreach (var item in _allRooms)
		{
			item.OnSetActive(item == _selectedRoom);
		}
	}

	public RoomInfo SelectedRoom
	{
		get
		{
			if (_selectedRoom != null)
			{
				return _selectedRoom.RoomInfo;
			}
			else
			{
				return null;
			}
		}
	}
}
