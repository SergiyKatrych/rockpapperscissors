﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomListItem : MonoBehaviour
{
	[SerializeField] private Text label;

	[SerializeField] private Color activeColor, unactiveColor;
 
	private Image _image;
	private RoomInfo _roomInfo;

	private RoomListController _roomListController;

	public void Init(RoomInfo roomInfo, RoomListController roomController)
	{
		_roomInfo = roomInfo;
		_roomListController = roomController;
		_image = GetComponent<Image>();
		label.text = _roomInfo.name;
	}

	public void OnClick()
	{
		_roomListController.OnSelectedRoom(this);
	}

	public void OnSetActive(bool isActive)
	{
		_image.color = isActive ? activeColor : unactiveColor;
	}

	public RoomInfo RoomInfo
	{
		get{ return _roomInfo; }
	}
}
